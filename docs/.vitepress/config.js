module.exports = {
  title: 'Hello Vitepress',
  description: 'Just playing around',
  dest: 'public',
  base: '/',
  darkMode: true,
  themeConfig: {
    sidebar: [
      {
        text: 'Home',
        link: '/'
      },
      {
        text: 'Getting Started',
        link: '/guide/getting-started.html',
      },
      {
        text: 'Components',
        link: '/components/readme.html',
        children: [
          {
            text: 'Button',
            link: '/components/button.html'
          }
        ]
      }
    ]
  }
}
