---
lang: en-US
title: Button
description: information about enso UI Button Component
---
# Button
<script setup>
import { EButton } from '../../lib/main'
</script>

<EButton text="I am a button"/>
