import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from 'path'
import dtsPlugin from 'vite-plugin-dts'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(),
    dtsPlugin()
  ],
  resolve: {
    alias: [{
      find: '@', replacement: '/src'
    }]
  },
  build: {
    lib: {
      entry: path.resolve(__dirname, 'lib/main.js'),
      name: 'enso',
      formats: ['es', 'cjs', 'umd'],
      fileName: (format) => `enso.${format}.js`
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: ['vue'],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: 'Vue'
        }
      }
    }
  }
})
